package com.example.m100266177.assignment1_marcevans100266177;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Created by 100266177 on 09/10/2015.
 */
public class AskQuestion extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ask_question);

        // Get the message from the intent
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainMenu.MESSAGE);

        TextView textView = (TextView) findViewById(R.id.textView2);
        textView.setText(message);

    }


    public void handleYes(View v){
        resultReturn("1");
    }

    public void handleNo(View v){
        resultReturn("0");
    }

    public void resultReturn(String number)
    {
        Intent resultIntent = new Intent(Intent.ACTION_PICK);
        resultIntent.putExtra("TEST", number);
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
