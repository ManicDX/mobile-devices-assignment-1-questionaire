package com.example.m100266177.assignment1_marcevans100266177;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Created by 100266177 on 15/10/2015.
 */
public class Summary extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);

        int yes = 0;
        int no = 0;

        Intent intent = getIntent();
        boolean[] answers = intent.getBooleanArrayExtra(MainMenu.ANSWERS);

        //Log.i("intents demo", "Loop" + answers.length);
        for(int i = 0; i < answers.length; i++)
        {
            if(answers[i] == true)
                yes++;
        }
        no = answers.length - yes;

        //print to summary textfields
        TextView textView = (TextView) findViewById(R.id.textView5);
        textView.append(String.valueOf(yes));
        TextView textView2 = (TextView) findViewById(R.id.textView6);
        textView2.append(String.valueOf(no));


    }

    public void handleClose(View view)
    {
        finish();
    }
}
