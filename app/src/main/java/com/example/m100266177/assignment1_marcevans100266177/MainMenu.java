package com.example.m100266177.assignment1_marcevans100266177;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class MainMenu extends Activity {

    public static int QUESTION_RESULT = 4111;//0100010;
    Resources res;
    String[] questions;
    boolean[] answers;
    int numAsked = 0;
    public final static String MESSAGE = "Message to AskQuestion";
    public final static String ANSWERS = "Message to Summary";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //get questions array
        res = getResources();
        questions = res.getStringArray(R.array.questions);

        answers = new boolean[questions.length];
        for(boolean b : answers)//initialize array
            b = false;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Button handler
    public void handleQuestionButton(View v){

        //Log.i("intents demo", "NumAsked: " + numAsked + " Question Length: " + questions.length);

        //if no more questions to ask, goto summary, else ask a question
        if(numAsked >= questions.length) {
            Intent intent = new Intent(this, Summary.class);
            //Put answers yes and no to summary
            intent.putExtra(ANSWERS, answers);
            startActivity(intent);
            finish();
        }
        else
        {
            Intent intent = new Intent(this, AskQuestion.class);
            //Get current question sent to AskQuestion
            String message = questions[numAsked];
            intent.putExtra(MESSAGE, message);
            startActivityForResult(intent, QUESTION_RESULT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent resultIntent){
        super.onActivityResult(requestCode, responseCode, resultIntent);

        //Get result of either yes or no from askQuestion, and add to yes/no array
        if(responseCode == RESULT_OK){

            String inc = resultIntent.getStringExtra("TEST");
            int temp = Integer.parseInt(inc);
            if(temp == 1)
                answers[numAsked] = true;
            else if(temp == 0)
                answers[numAsked] = false;
            numAsked++;
        }
    }
}
